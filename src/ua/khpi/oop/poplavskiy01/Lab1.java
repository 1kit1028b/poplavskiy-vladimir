package ua.khpi.oop.poplavskiy01;
/**
 
* @mainpage
 * ����������� ������ �1 
 * ����:��������� �������� ����� Java.���� �����,�������,�������� � ��������� 

*������ �� ������:
1.	������� ��� ��������� ������ �� ��� Java � ���������� Eclipse.
2.	���������������� ��������� ��������� �������� �� ���������� ������ � ����� ������������, �� �������������� ��������� �� ������.
3.	�������� ��������� � ������ �������� � ���������� ����� �� ��������� ��������� ����� JDK.

* @author Poplavskiy Vova
* @version 1.0 
* @data 29/09/19 
*/

/**
 * �� Main �������� ����c ���������, ������: ����� main ����� numberOfEven
 * ����� numberOfOdd ����� binary ����� NumberOfOnes ������������ ��������� ��
 * 1. ���������� ������ 2. ���������� ���� ����� � ����� �������� ��������
 * 3. ��������� ��������� ����� ��� ������� ����� "15" 4. ϳ��������M����
 * main ������� �������
 * <p>
 * �� ������� ������ � �� ������ ����� � ����� ��������� ������� ������� �
 * ��������� ����
 * 
 * @return ��� void ������ �� �������. ������ ���������� ��������.
 */

public class Lab1 {
	/**
	 * M���� main ������� ������� * @author Poplavskiy Vova
	 * 
	 * @version 1.0
	 * @data 29/09/19
	 */

	public static void main(String[] args) {

		int record_book = 0x4690; // ����� ���. ������ � 16���. �������
		long phoneNumb = 3809847256215L;// ����� �������� � ����
		int twoPhoneNum = 0b1111;// 15 � 2��. �������
		int fourPhoneNum = 014107;// 6215 � 8���. �������
		int j = 14; // ����� ������� �� ������ -1
		int j1 = (j % 26) + 1;// ������� �� �������
		char letter = Lab1.getLetter(j1);

		// System.out.println("j mod 26 = " + j1); // ����� �������
		// System.out.println(" letter = " + letter); // ����� �������

		int a = numberOfEven(record_book); // ���������� ������
		a = numberOfOdd(record_book);// ���������� ��������

		a = numberOfEven(phoneNumb);// ���������� ������
		a = numberOfOdd(phoneNumb);// ���������� ��������

		a = numberOfEven(twoPhoneNum);// ���������� ������
		a = numberOfOdd(twoPhoneNum);// ���������� ��������

		a = numberOfEven(fourPhoneNum);// ���������� ������
		a = numberOfOdd(fourPhoneNum);// ���������� ��������

		a = numberOfEven(j1);// ���������� ������
		a = numberOfOdd(j1);// ���������� ��������

		String b = binary(record_book); // ������� ����� � �������� �������
		int c = numberOfOne(b); // ���������� ������

		b = binary(phoneNumb);// ������� ����� � �������� �������
		c = numberOfOne(b);// ������� ����� � �������� �������

		b = binary(twoPhoneNum);// ������� ����� � �������� �������
		c = numberOfOne(b);// ������� ����� � �������� �������

		b = binary(fourPhoneNum);// ������� ����� � �������� �������
		c = numberOfOne(b);// ������� ����� � �������� �������

		b = binary(j1);// ������� ����� � �������� �������
		c = numberOfOne(b);// ������� ����� � �������� �������

	}

	public static int numberOfEven(long n) {
		int count = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 == 0)
				count++;
			t /= 10;
		}
		return count;
	}

	public static int numberOfOdd(long n) {
		int count = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 != 0)
				count++;
			t /= 10;
		}
		return count;
	}

	public static String binary(long a) {
		long b;
		String temp = "";
		while (a != 0) {
			b = a % 2;
			temp = b + temp;
			a = a / 2;
		}
		return temp;
	}

	public static int numberOfOne(String a) {
		int count = 0;
		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == '1')
				count++;
		}
		return count;
	}

	public static char getLetter(int j1) { // ����� ��� �����
		return (char) (((j1 - 1) % 26) + 'A');

	}
}