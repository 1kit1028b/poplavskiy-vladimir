package ua.khpi.oop.poplavskiy05;


import java.io.Serializable;
import java.util.Iterator;

import ua.khpi.oop.poplavskiy05.Container;

public class ContainerIterator implements Iterator<String>, Serializable {
	Container container;
	int currentNumber = 0;

	public ContainerIterator(Container container) {
		this.container = container;
	}

	@Override
	public boolean hasNext() {
		return currentNumber < this.container.mas.length;
	}

	@Override
	public String next() {
		/*
		 * String string = current.getData(); current = current.getNext();
		 */
		return this.container.mas[currentNumber++];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}