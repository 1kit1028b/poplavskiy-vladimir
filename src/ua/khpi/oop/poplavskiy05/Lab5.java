
package ua.khpi.oop.poplavskiy05;


/**
 * @mainpage
 * ����������� ������ �5
 * �������� ������� ����������. ��������� 
 * ����: ������� ������� �������� ������� ���������� �� ������������ ���������. 
 *  ������
 * 1.	��������� ����-���������, �� ��������� ��� ���������� ���������� ����� �������� �.�. �3 � ������ ������ ����� � ��������� ���������, ��������� � ���� ��������. 
 * 2.	� ��������� ���������� �� ���������������� �������� ������: 
 * �	String toString() ������� ���� ���������� � ������ �����; 
 * �	void add(String string) ���� �������� ������� �� ���� ����������; 
 * �	void clear() ������� �� �������� � ����������; 
 * �	boolean remove(String string) ������� ������ ������� ��������� �������� � ����������; 
 * �	Object[] toArray() ������� �����, �� ������ �� �������� � ���������; 
 * �	int size() ������� ������� �������� � ���������; 
 * �	boolean contains(String string) ������� true, ���� ��������� ������ �������� �������; 
 * �	boolean containsAll(Container container) ������� true, ���� ��������� ������ �� �������� � ����������� � ����������; 
 * �	public Iterator<String> iterator() ������� �������� �������� �� Interface Iterable. 
 * 3.	� ���� ��������� �������� �� Interface Iterator ���������� ������: 
 * �	public boolean hasNext(); 
 * �	public String next(); 
 * �	public void remove(). 
 * 4.	���������������� ������ ��������� �� ��������� ����� while � for each. 
 * 5.	������������� ������������ ���������� (��������) � ��������� � Java Collections Framework.
 *
 * @author Poplavskiy Vladimir
 * @version 1.0 
 * @data 17/12/19
 * 
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Poplavskiy Vladimir
 * @version 1.0 17/12/19
 */

public class Lab5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Container container = new Container();
		Container container1 = new Container();
		container.add("Today is very good weather");
		container.add("How many vowels are there in this sentence?");
		container.add("How many words are there in this sentence?");
		container.add("Unnecessary sentence");

		container1.add("Today is very good weather");
		container1.add("How many vowels are there in this sentence?");
		container1.add("How many words are there in this sentence?");

		boolean contains = container.contains("Today is very good weather");
		// container.clear();
		container.remove("Unnecessary sentence");
		//boolean containsAll = container.containsAll(container, container1);
		// container.toArray();
		for (String string : container)
			System.out.println(string);

		ContainerIterator it = container.iterator();
		while (it.hasNext()) {
			// it.next();
			System.out.println(it.next());
		}

	}

}
