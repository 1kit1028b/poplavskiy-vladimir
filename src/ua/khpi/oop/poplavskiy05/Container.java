package ua.khpi.oop.poplavskiy05;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Scanner;

public class Container implements Iterable<String>, Serializable {
	final int size = 1;
	String mas[] = new String[size];
	String current = mas[0];
	int currentSize = 0;
	
	public int getCurrentSize() {
		return currentSize;
	}

	@Override
	public ContainerIterator iterator() {
		return new ContainerIterator(this);
	}
	
	
	
	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder(100);
		for(int i = 0; i < (mas.length); i++) {
			strBuilder.append(mas[i]).append('\n');
		}
		return strBuilder.toString();
	}
	
	

	public void add(String string) {
	    mas = Arrays.copyOf(mas, currentSize+1);
	    mas[currentSize] = string;
		currentSize++;
	}

	
	
	
	public void clear() {
		mas = new String[size];
	}
	
	
	
	boolean remove(String string) {
		currentSize = size();
		for (int i = 0; i < currentSize; i++) {
			if (mas[i] == string) {
				mas[i] = null;
				break;
			}
		}
		return false;
	}
    
	public Object[] toArray() {
		Object[] object = mas;
		return object;
	}
	
	
	
	public int size() {
		int currentSize1 = 0;
		for (int i = 0; i < mas.length; i++) {
				currentSize1++;
		}
		return currentSize1;
	}
	
	
		boolean contains(String string) {
		for (int i = 0; i < currentSize; i++) {
			if (mas[i].equals(string))
				return true;
		}
		return false;
	}
	
	
	
	boolean containsAll(String[] strings) {
		if(strings.length > currentSize) {
			return false;
		}
		for(int i = 0; i < currentSize && i < strings.length; i ++) {
			if(!mas[i].equals(strings[i])) {
				return false;
			}
		}
		return true;
	}
	
	public void scanInfo() {
		Scanner in = new Scanner(System.in);
		add(in.nextLine());
	}
	
	public int getIndexOfElem(String str) {
		if(!contains(str)) {
			return -1;
		}
		
		for(int i = 0; i < str.length(); i++) {
			if(mas[i].equals(str)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public String getElemByIndex(int index) {
		if(index >= currentSize) {
			return "";
		}
		return mas[index];
	}
	
	public boolean equals(String[] first) {
		if(first.length != currentSize) {
			return false;
		}
		for(int i = 0; i < first.length; i++) {
			if(!first[i].equals(mas[i])) {
				return false;
			}
		}
		return true;
	}
	
	public void sort() {
		Arrays.sort(mas);
	}
}
