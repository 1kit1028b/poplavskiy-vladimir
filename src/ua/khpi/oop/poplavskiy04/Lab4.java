package ua.khpi.oop.poplavskiy04;

import java.util.Arrays;
import java.util.Scanner;

public class Lab4 {

	public static void main(String[] args) {

		boolean contains1 = Arrays.asList(args).contains("help");
		boolean contains2 = Arrays.asList(args).contains("debug");

		if (contains1 == true) {
			System.out.println("����������� ������ �4\r\n"
					+ "  ����: ������������ ��������� �������� ��� ��������� Java SE\r\n" + "  \r\n"
					+ "  ����: ��������� ���������� ������ ������ � ������������ � ���������� ��������� ����� Java.\r\n"
					+ "  ������: \r\n"
					+ "  1) �������������� �������� ������ �������� ����������� ������ �3, �������� �� ��������� ������ ����������� ������� ������ ����������� � ������ ���������� ����:\r\n"
					+ "  �������� �����;\r\n" + "  �������� �����;\r\n" + "  ��������� ���������;\r\n"
					+ "  ����������� ����������;\r\n" + "  ���������� �������� � �.�.\r\n"
					+ "  2) ����������� ������� ��������� ���������� ����� ��� ���������� ������ ������ ��������:\r\n"
					+ "  �������� �-h� �� �-help�: ������������ ���������� ��� ������ ��������, ����������� (������������ ��������), ��������� ���� ������ ������ (������ ���� �� ��������� ���������� �����);\r\n"
					+ "  �������� �-d� �� �-debug�: � ������ ������ �������� ������������� �������� ����, �� ���������� ������������ �� �������� ������������� ��������: ����������� �����������, ������� �������� ������, �������� ���������� ������ �� ��.\n"
					+ "  �����: ����������� ���������\n");
		}

		if (contains2 == true) {
			System.out.println("��������� �������� � ������ debug");
		}
		String str = null;
		while (true) {
			System.out.println("1. ����� ������ ����� ������� 1");
			System.out.println("2. ���� ������ ������ ������ ������� 2");
			System.out.println("3. ���� ������ ������ ���������� ������� ������� 3");
			System.out.println("4. ���� ������ ������ ���������� ��������� ������� 4");
			System.out.println("5. ���� ������ ������ ���� ������ ������� ��� ��������� ������� 5");
			System.out.println("6. ���� ������ ������ ���������� ���� �������� 6");
			System.out.println("7. ��� ������ ������� 7");

			
			Scanner in = new Scanner(System.in);
			System.out.print("��� �����:");
			int x = in.nextInt();

			int countVow = 0;
			int countlet = 0;
			//countVow = vowels(str, countVow);
			//countlet = letters(str, countlet);

			//int c = countlet - countVow;

			switch (x) {
			
			case 1:
				Scanner scan = new Scanner(System.in);
				System.out.print("������� �����������: ");
				 str = scan.nextLine();
				 break;
			
			case 2:
				StringBuffer strBuffer = new StringBuffer(str);
				System.out.println("�����: " + strBuffer.length());
				break;
			case 3:
				countVow = vowels(str, countVow);
				System.out.println("���������� �������:" + countVow);
				break;
			case 4:
				countlet = letters(str, countlet);
				countVow = vowels(str, countVow);
				int n = countlet - countVow;
				System.out.println("���������� ���������:" + n);
				break;
			case 5:
				countlet = letters(str, countlet);
				countVow = vowels(str, countVow);
				int c = countlet - countVow;
				if (c < countVow)

				{
					System.out.println("������� ������: " + countVow);
				}

				if (countVow < c) {
					System.out.println("co������� ������: " + c);
				}

				break;
			case 6:
				countlet = letters(str, countlet);
				System.out.println("���������� ����:" + countlet);
				break;
			case 7:
				System.out.println("���������� ���������...");
				System.exit(0);
				break;
			default:
				System.out.println("�� ����� �������� �������� ����...\n");
			}
		}

	}

	public static int vowels(String s, int c) {
		c = 0;
		for (char elem : s.toCharArray()) {
			if (elem == 'a') {
				c++;
			}
			if (elem == 'e') {
				c++;
			}

			if (elem == 'i') {
				c++;
			}
			if (elem == 'u') {
				c++;
			}

			if (elem == 'o') {
				c++;
			}
		}
		return c;
	}

	public static int letters(String s, int l) {
		l = 0;
		for (char let : s.toCharArray()) {
			if (let != ' ') {
				l++;
			}
		}

		return l;
	}

}
