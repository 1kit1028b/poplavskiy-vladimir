package ua.khpi.oop.poplavskiy06;

import java.util.List;
import java.util.Scanner;

import ua.khpi.oop.poplavskiy05.Container;

public class Menu extends HelperMenu {

	public void start(Container obj) {
		Menu(obj);
	}

	private void Menu(Container obj) {
		Scanner in = new Scanner(System.in);
		int x = 0;
		do {
			System.out.println("Hello!What do you want to chose?: ");
			System.out.println("1 - Add element to container");
			System.out.println("2 - Search ");
			System.out.println("3 - Show all");
			System.out.println("4 - Save or restore");
			System.out.println("5 - Sort");
			System.out.println("6 - Processing elements by own class and borrowed class");
			System.out.println("7 - Invoke 'wordCounter' method from borrowed class");
			System.out.println("8 - Clean all");
			System.out.println("9 - Compare ");
			System.out.println("10- Selective processing elements by own class and borrowed class ");
			System.out.println("0 - Exit");
			System.out.print("Your choise:");
			x = in.nextInt();

			switch (x) {
			case 1: {
				System.out.print("Write a string:");
				obj.scanInfo();
				break;
			}

			case 2: {
				int y = 0;

				System.out.println("Do you want to search data by string or index?(1 or 2)\n" + "Write a number");

				y = in.nextInt();
				switch (y) {
				case 1: {
					System.out.println("Write string");
					String data = scanLine();
					search(obj, 0, data, 0);
					break;
				}

				case 2: {
					System.out.println("Enter index");
					int index = in.nextInt();
					search(obj, 1, "", index);
					break;
				}
				}
				break;
			}

			case 3: {
				System.out.println("Data: ");
				System.out.println(obj.toString());
				break;
			}

			case 4: {
				int z = 0;

				System.out.println("Do you want to save or restore data?(1 or 2)");
				System.out.println("Write 1 or 2");

				z = in.nextInt();
				switch (z) {
				case 1: {
					System.out.println("Enter a name or file way:");
					String way = scanLine();
					save(obj, way);
					System.out.println("Data is saved to file: " + way);
					break;
				}

				case 2: {
					System.out.println("Enter a name or file way:");
					String way = scanLine();
					obj = (Container) restore(obj, way);
					System.out.println("Data was restored from file: " + way);
					break;
				}
				}
				break;
			}

			case 5: {
				obj.sort();
				System.out.println("Elements are sorted");
				break;
			}

			case 6: {
				System.out.println("Enter a mode:");
				System.out.println("0 - own");
				System.out.println("1 - borrowed");
				int ownOrBorrowed = 0;
				ownOrBorrowed = in.nextInt();
				processingAllElem(obj, ownOrBorrowed);
				break;
			}

			case 7: {
				System.out.println("Enter a words divided by space to count them:");
				String input = scanLine();
				int i = 0;
				List<String> stringList = ua.khpi.oop.mokropulo03.Helper.tokenize(input, ' ');
				ua.khpi.oop.mokropulo03.Helper.wordCounter(stringList);
				break;
			}

			case 8: {
				obj.clear();
				System.out.println("All data deleted");
				break;
			}

			case 9: {
				DataComparing(obj);
				break;
			}

			case 10: {
				System.out.println("Enter a mode:");
				System.out.println("0 - own");
				System.out.println("1 - borrowed");
				int ownOrBorrowed = 0;
				ownOrBorrowed = in.nextInt();
				selectiveProccessing(obj, ownOrBorrowed);
				break;
			}

			case 0: {
				System.out.println("Closing program.Goodbye!");
				in.close();
				break;
			}

			}

		} while (x != 0);
	}
}
