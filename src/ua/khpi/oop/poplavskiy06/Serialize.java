package ua.khpi.oop.poplavskiy06;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialize {
	
	 void serializing(Object obj, String wayToFile) {
		System.out.println("Serializing");
		try {
			FileOutputStream fos = new FileOutputStream(wayToFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
	        oos.flush();
	        oos.close();
	        fos.flush();
	        fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	 Object deserializing(String wayToFile) {
		System.out.println("Deserializing");
		Object obj = null;
		try {
			FileInputStream fis = new FileInputStream(wayToFile);
			ObjectInputStream ois = new ObjectInputStream(fis);	
			obj = ois.readObject();
			
			fis.close();
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return obj;
	}
}

