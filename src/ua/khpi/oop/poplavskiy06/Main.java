package ua.khpi.oop.poplavskiy06;

import ua.khpi.oop.poplavskiy05.Container;

public class Main {

	public static void main(String[] args) {
		Container container1 = new Container();
        Menu menu = new Menu();
        menu.start(container1);
	}

}
