package ua.khpi.oop.poplavskiy06;


import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import ua.khpi.oop.poplavskiy03.Helper;
import ua.khpi.oop.poplavskiy05.Container;

class HelperMenu {

	
	protected String scanLine() {
		Scanner in = new Scanner(System.in);
		String way = in.nextLine();
		
		return way;
	}
	

	protected void processingAllElem(Container obj, int x) {
		if(x == 1) {
			borrowedProccessing(obj);
			return;
		} else if (x == 0){
			ownProccessing(obj);
			return;
		}
		System.out.println("Try again, it can be 0 or 1");
	}
	
	protected void ownProccessing(Container obj) {
		final int hold = 100;
		StringBuilder strBuilder = new StringBuilder(hold);
		String info = obj.toString();
		for(int i = 0; i < info.length(); i++) {
			if(info.charAt(i) != '\n') {
				strBuilder.append(info.charAt(i));
			} else {
				int c = 0;
				System.out.println(
						"In " 
				        + strBuilder.toString() 
						+ " word " 
						+ ua.khpi.oop.poplavskiy03.Helper.letters(strBuilder.toString(), c) + 
						" letters");
				strBuilder.replace(0, strBuilder.toString().length(),"");
			}
		}
	}
	
	protected void borrowedProccessing(Container obj) {
		List<String> stringList = new ArrayList<>();
		stringList = ua.khpi.oop.mokropulo03.Helper.tokenize(obj.toString(), '\n');
		ua.khpi.oop.mokropulo03.Helper.wordCounter(stringList);

	}
	
	protected void save(Container obj, String wayToFile) {
		Serialize serialize = new Serialize();
		serialize.serializing(obj, wayToFile);
	}
	
	protected Object restore(Object obj, String wayToFile) {
		Serialize serialize = new Serialize();
		obj = serialize.deserializing(wayToFile);
		return obj;
	}
	
	
	

	
	protected void DataComparing(Container obj) {
		Scanner in = new Scanner(System.in);
		List<String> stringList = new ArrayList<>();
		System.out.println("When you finish choose 0");
		do {
			System.out.println("Enter your string:");
			String data = in.nextLine();
			if(data.equals("0")) {
				break;
			}
			stringList.add(data);
			
		} while(true);
		String[] data = Arrays.copyOf(stringList.toArray(), stringList.toArray().length, String[].class);
		if(obj.equals(data)) {
			System.out.println("Arrays are equals");
		} else {
			System.out.println("Arrays aren't equals");
		}
	}
	
	protected void selectiveProccessing(Container obj, int x) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter index of element to proccessing");
		int index = in.nextInt();
		String result = obj.getElemByIndex(index);
		if(result.equals("")) {
			System.out.println("\n There is no string fo this index");
			return;
		}
		if(x == 0) {
			selectiveOwnProccessing(obj, result);
		} else if (x == 1) {
			selectiveBorrowedProccessing(obj, result);
		}
	}
	
	protected void selectiveOwnProccessing(Container obj, String str) {
		int c = 0;
		System.out.println(
				"In " 
		        + str
				+ " word " 
				+ Helper.letters(str, c)
				+ " letters");
	}
	
	protected void selectiveBorrowedProccessing(Container obj, String str) {
		List<String> stringList = new ArrayList<>();
		stringList.add(str);
		ua.khpi.oop.mokropulo03.Helper.wordCounter(stringList);
	}



protected void search(Container obj, int x, String data, int index) {
	if(x == 0) {
		int result = 0;
		result = searchByString(obj, data);
		System.out.println("Index of " + data + ": " + result);
		return;
	} else if (x == 1) {
		String result = searchByIndex(obj, index);
		System.out.println("Element by index " + index + " is " + result);
		return;
	}
	System.out.println("Try again, it can be 0 or 1");
}

protected int searchByString(Container obj, String data) {
	return obj.getIndexOfElem(data);
}

protected String searchByIndex(Container obj, int index) {
	return obj.getElemByIndex(index);
}

}
